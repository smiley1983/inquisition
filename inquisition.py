#!/usr/bin/python2
#
#    Python Inquisitor - asks questions according to a script, records answers
#    Copyright (C) 2011 Jude Hungerford
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
##############################################################################

import Tkinter
import string

default_min_entry = 3
NIL = 0
CHOICE = 1
RBUTTON = 2
TEXTBOX = 3
NUMRB = 4
MINTEXT = 5
PAGEBREAK = 6
LONGTEXT = 7

t_choice = 'choice'
t_rbutton = 'option'
t_numrb = 'multinumeric'
t_textbox = 'textbox'
t_mintext = 'mintext'
t_pagebreak = 'break'
t_longtext = 'longinput'

def is_integer(s):
   try:
      int(s)
      return True
   except ValueError:
      return False

def lb_filter(s):
   result = ""
   for c in s:
      if c == "\n":
         result = result + "\\n"
      else:
         result = result + c
   return result

class Lexer:
   def tokenize(self, s):
      result = []
      buf = ""
      inquote = False
      commented = False
      escaped = False
      for letter in s.rstrip():
       if not commented:
         if letter == '"':
            buf = buf + '"'
            if inquote:
               result.append(buf)
               buf = ""
            inquote = not inquote
         elif inquote and (letter == "\\"):
            escaped = True
         elif inquote and escaped and letter == "n":
            buf = buf + "\n"
            escaped = False
         elif not inquote and letter == '#':
            commented = True
         elif not inquote and letter == ' ':
            if not buf == "":
               result.append(buf)
               buf = ""
         elif not inquote and letter == ',':
            if not buf == "":
               result.append(buf)
               buf = ""
         elif not inquote and letter == '=': # is this used? no...
            if not buf == "":
               result.append(buf)
               buf = ""
            result.append('=')
         else: 
            buf = buf + letter
      if not buf == "":
         result.append(buf.lstrip().rstrip())
      return result
   def verify(self, l, ln):
      result = []
      for item in l:
         if item.lower() == t_choice:
            result.append(t_choice)
         elif item.lower() == t_rbutton:
            result.append(t_rbutton)
         elif item.lower() == t_numrb:
            result.append(t_numrb)
         elif item.lower() == t_textbox:
            result.append(t_textbox)
         elif item.lower() == t_longtext:
            result.append(t_longtext)
         elif item.lower() == t_mintext:
            result.append(t_mintext)
         elif item.lower() == t_pagebreak:
            result.append(t_pagebreak)
         elif is_integer(item):
            result.append(item)
         elif item[0] == '"' and item[(len(item) - 1)] == '"' and len(item) > 1:
            result.append(item.lstrip('"').rstrip('"'))
         else:
            print ("Warning (lexer): line " + str(ln) + ': unrecognized token "' + item + '"' )
      return result
   def group(self, t):
      result = []
      buf = []
      for item in t:
         if item == t_choice or item == t_textbox or item == t_longtext:
            if not buf == []:
               result.append(buf)
            buf = [item]
         else:
            buf.append(item)
      if not buf == []:
         result.append(buf)
      return result
   def read_file(self, s):
      chan = open(s, 'r') # note that this DOES NOT catch exceptions
      count = 0
      result = []
      for line in chan:
         count = count + 1
         tokens = self.tokenize(line)
         vt = self.verify(tokens, count)
         result.extend(vt)
      return result
   def __init__(self, fname):
      tokens = self.read_file(fname)       
      self.l = self.group(tokens)

class Writer:
   def __init__(self, fname, mode):
      if mode == "a":
         self.first = False
      else:
         self.first = True
      self.chan = open(fname, mode)
   def write(self, s):
      if not self.first:
         self.chan.write(",")
      else:
         self.first = False
      if is_integer(s):
         self.chan.write(s)
         self.chan.flush()
      else:
         self.chan.write('"' + s + '"')
   def close(self):
      self.chan.close()

class Question:
   def __init__(self, n, check_proceed):
      self.v = Tkinter.StringVar()
      self.q = n
      self.valid = False
      self.min_entry = default_min_entry
      self.breakpoint = False
      self.action = NIL
      self.check_proceed = check_proceed
      self.is_longtext = False
   def interpret(self, master):
      self.v = Tkinter.StringVar()
      for token in self.q:
         if token == t_choice:
            self.action = CHOICE
         elif token == t_rbutton:
            self.action = RBUTTON
         elif token == t_numrb:
            self.action = NUMRB
         elif token == t_textbox:
            self.action = TEXTBOX
         elif token == t_longtext:
            self.action = LONGTEXT
         elif token == t_mintext:
            self.action = MINTEXT
         elif token == t_pagebreak:
            self.breakpoint = True
         elif self.action == CHOICE:
            self.action = NIL
            l = Tkinter.Label(master, text = token, justify=Tkinter.LEFT, wraplength=1000)
            l.pack(anchor=Tkinter.NW, side=Tkinter.TOP)
         elif self.action == RBUTTON:
            self.action = NIL
            rb = Tkinter.Radiobutton (master, text=token, variable=self.v,
                                      value=token, command=self.select)
            rb.pack(anchor=Tkinter.N, side=Tkinter.LEFT)
         elif self.action == NUMRB:
            self.action = NIL
            for item in range(1, ((int(token)) + 1)):
               rb = Tkinter.Radiobutton (master, text=str(item),
                                         variable=self.v, value=item,
                                         command=self.select)
               rb.pack(anchor=Tkinter.N, side=Tkinter.LEFT)
         elif self.action == TEXTBOX:
            self.action = NIL
            self.min_entry = default_min_entry
            self.textbox(token, master)
#            l = Tkinter.Label(master, text = token)
#            l.pack(side=Tkinter.TOP)
#            textbox = Tkinter.Entry(master, width = 60, 
#                                    textvariable=self.v)
#            textbox.bind("<Key>", self.minlength)
#            self.entercount = 0
#            textbox.pack(side=Tkinter.TOP)
         elif self.action == LONGTEXT:
            self.is_longtext = True
            self.action = NIL
            self.min_entry = default_min_entry
            self.longtext(token, master)
         elif self.action == MINTEXT:
            self.action = NIL
            self.min_entry = int(token)
            if self.min_entry == 0:
               self.valid = True
   def textbox(self, t, master):
      l = Tkinter.Label(master, text = t, justify=Tkinter.LEFT, wraplength=1000)
      l.pack(anchor=Tkinter.NW, side=Tkinter.TOP)
      textbox = Tkinter.Entry(master, width = 60, 
                              textvariable=self.v)
      textbox.bind("<Key>", self.minlength)
      textbox.pack(anchor=Tkinter.NW, side=Tkinter.TOP)
   def longtext(self, t, master):
      l = Tkinter.Label(master, text = t, justify=Tkinter.LEFT, wraplength=1000)
      l.pack(anchor=Tkinter.NW, side=Tkinter.TOP)
      textbox = Tkinter.Text(master, width = 60)
      textbox.bind("<Key>", self.minlength)
      textbox.pack(anchor=Tkinter.NW, side=Tkinter.TOP)
      self.v = textbox
   def select(self):
      self.valid = True
      self.check_proceed()
   def minlength(self, val): # used to check if enough chars have been entered
      if len(self.get_result()) >= (self.min_entry - 1):
         self.valid = True
         self.check_proceed()
      else:
         self.valid = False
         self.check_proceed
   def get_result(self):
      if self.is_longtext:
#         return self.v.get()
         return lb_filter(self.v.get('1.0', 'end -1 chars'))
      else:
         return self.v.get()

class Inquisitor(Tkinter.Frame):
   def load_prev(self, fname, maxlen):
      try:
         prev = Lexer(fname)
         thislen = len(prev.l[0])
         if thislen == 0:
            raise IOError
         elif thislen == maxlen:
            msg1a = "There is already a completed file for that ID:\n"
            msg1b = "please talk to the admin if you want to start over."
            self.confirm_exit(msg1a + msg1b)
         else:
            print "loading previous"
            self.continue_prev(fname, thislen)
      except:
        self.first(fname)
   def confirm_exit(self, msg):
      for item in self.master.pack_slaves():
         item.destroy()
      l = Tkinter.Label(self.master, text = msg)
      l.pack(anchor=Tkinter.N)
      self.okb = Tkinter.Button(self.master, text="OK", command=self.quit)
      self.okb.pack(anchor=Tkinter.N, side=Tkinter.RIGHT)
      Tkinter.mainloop()
   def quit(self):
      self.master.destroy()
   def ask_next(self):
      if not self.count < 0 and not self.resume:
         for c in range(((self.count - self.set_len)), self.count):
            self.writer.write(self.question[c].get_result())
#            self.writer.write((self.question[c].v).get())
      if self.resume:
         self.resume = False
      if self.count >= (len(self.program.l) ):
         self.writer.close()
         self.confirm_exit("You have finished. Thankyou for your time.")
#         self.master.destroy()
      else:
         for item in self.master.pack_slaves():
            item.destroy()
         self.ask()
   def add_questions(self):
      done = False
      self.set_len = 0     
#      self.count = self.count + 1
      if self.count == -1:
         self.count = 0
      while not done:
         if self.count < self.maxlen: # messy FIXME
            qframe = Tkinter.Frame()
            self.question[self.count].interpret(qframe)
            qframe.pack(fill=Tkinter.X, padx=5, pady=5)
            separator = Tkinter.Frame(height=2, bd=1, relief=Tkinter.SUNKEN)
            separator.pack(anchor=Tkinter.NW, fill=Tkinter.X, padx=5, pady=5)
         if self.count >= (self.maxlen - 1):
            self.count = self.maxlen - 1
            done = True
         elif self.question[self.count].breakpoint:
            done = True
         self.set_len = self.set_len + 1
         self.count = self.count + 1
   def ask(self):
#      v = Tkinter.StringVar()
#      self.question[self.count].interpret(self.master)
      self.add_questions()
      self.okb = Tkinter.Button(self.master, text="OK", command=self.ask_next, 
                           state=Tkinter.DISABLED)
      self.okb.pack(anchor=Tkinter.N, side=Tkinter.RIGHT)
#      if self.question[self.count].min_entry == 0: # FIXME
#         self.okb.config(state=Tkinter.ACTIVE)
      Tkinter.mainloop()
   def check_proceed(self):
      proceed = True
      for c in range((self.count - self.set_len), (self.count)):
         if not self.question[c].valid:
            proceed = False
      if proceed:
         self.okb.config(state=Tkinter.ACTIVE)
      else:
         self.okb.config(state=Tkinter.DISABLED)
   def select(self):
      self.okb.config(state=Tkinter.ACTIVE)
   def minlength(self, val): # used to check if enough chars have been entered
      self.entercount = self.entercount + 1
#      if self.entercount > 2 or len(self.v.get()) > 3:
      if len(self.v.get()) >= (self.min_entry - 1):
         self.okb.config(state=Tkinter.ACTIVE)
      else:
         self.okb.config(state=Tkinter.DISABLED)
   def mloop(self):
      master.mainloop()
   def textbox(self, t):
      l = Tkinter.Label(self.master, text = t)
      l.pack(side=Tkinter.TOP)
      textbox = Tkinter.Entry(self.master, width = 60, 
                              textvariable=self.v)
      textbox.bind("<Key>", self.minlength)
      self.entercount = 0
      textbox.pack(side=Tkinter.TOP)
   def id_okbutton(self):
      self.okb = Tkinter.Button(self.master, text="OK", 
                                command=self.confirm_id, 
                                state=Tkinter.DISABLED)
      self.okb.pack(anchor=Tkinter.N, side=Tkinter.RIGHT)
   def first(self, fname):
      self.writer = Writer(fname, "w")
      self.action = NIL
      self.ask_next()
   def id_response(self):
      self.load_prev(self.userid + ".log", self.maxlen)
   def confirm_id(self):
      for item in self.master.pack_slaves():
         item.destroy()
      self.userid = self.v.get()
      msg = "Please confirm that this ID is correct, and close the program if not:\n" + self.userid
      l = Tkinter.Label(self.master, text = msg)
      l.pack(anchor=Tkinter.N)
      self.okb = Tkinter.Button(self.master, text="Yes, that's correct", 
                                command=self.id_response)
      self.okb.pack(anchor=Tkinter.N, side=Tkinter.RIGHT)
      Tkinter.mainloop()
   def confirm_id_okb(self):
      self.okb = Tkinter.Button(self.master, text="OK", 
                                command=self.confirm_id, 
                                state=Tkinter.DISABLED)
      self.okb.pack(anchor=Tkinter.N, side=Tkinter.RIGHT)
   def continue_prev(self, fname, progress):
      self.resume = True
      self.writer = Writer(fname, "a")
      self.action = NIL
      self.count = progress
      self.ask_next()
   def __init__(self, master, instruction):
      self.userid = ""
      self.maxlen = len(instruction.l)
      self.count = -1
      self.set_len = 0
      self.resume = False
#      self.entercount = 0
      self.v = Tkinter.StringVar()
      self.program = instruction
      self.question = []
      for q in instruction.l:
         self.question.append(Question(q, self.check_proceed))
      self.master = master
      self.min_entry = default_min_entry
      Tkinter.Frame.__init__(self, master)
      self.textbox("Please enter a unique ID string with no spaces:")
      self.id_okbutton()
      Tkinter.mainloop()
      
root = Tkinter.Tk()
width, height = root.maxsize()
root.geometry("%dx%d%+d%+d" % (width, height, 0, 0))
instruction = Lexer('input.txt')
inq = Inquisitor(root, instruction)
